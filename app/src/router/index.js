import Vue from "vue";
import VueRouter from "vue-router";
import SignIn from "../views/Signin.vue";
import ChallengesList from "../views/ChallengesList.vue";
import ChallengesDetail from "../views/ChallengeDetail.vue";
import Store from "../store/index"

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "signin",
    component: SignIn,
  },
    {
    path: "/challenges",
    name: "challenges",
    component: ChallengesList,
    meta: { requiresAuth: true },
  },
      {
    path: "/challenges/:id",
    name: "challengesDetail",
    component: ChallengesDetail,
    meta: { requiresAuth: true },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  let token = Store.getters.getToken;
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if ( token == null || token == "") {
            next({name : 'signin' })
        } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
