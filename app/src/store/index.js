import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import router from "../router/index"
import createPersistedState from "vuex-persistedstate";
import Cookies from 'js-cookie';

var url = "https://api.track13-ops01-1.oliv-it.com/";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userid : "",
    token : "",
    listeChallenges : [],
    listeChallengesDone : [],
    passwordGuess : ""
  },
  mutations: {
    setUserId(state, payload){
      state.userid = payload;
    },
    setPassword(state, payload){
      state.passwordGuess = payload;
    },
    setToken(state,payload) {
      state.token = payload;
    },
    setListeChallenges(state,payload) {
      state.listeChallenges = payload;
    },
    setListeChallengesDone(state,payload) {
      state.listeChallengesDone = payload;
    }
  },
  actions: {
    async connect(state, payload){
      axios({
        method: "post",
        url: url + "user/login",
        data: { 
          username: payload.login.username,
          password: payload.login.password,
        },
      }).then((response) => {
        if (response.data != "User not found") {
          this.commit("setUserId",response.data.id)
          this.commit("setToken",response.data.token)
          router.push("challenges");
        }
        return response;
      });
    },
    async loadChallenges(){
     axios({
      method: "get",
      url: url + "challenges/",
      headers: {
        Authorization: `Bearer ${this.getters.getToken}`,
      },
    })
      .then((response) => {
        if (response.data != "") {
          this.commit("setListeChallenges", response.data) ;
        }
      })
      .catch((error) => {
        console.log(error);
      });

    },

     async passwordGuess(state, payload){
         var guess = await axios({
        method: "post",
        url: url + "challenges/password",
        data: {
          password: payload.pass.password,
          challengeid : payload.pass.challengeid

        },
        headers: {
        Authorization: `Bearer ${this.getters.getToken}`,
      },
      }).then((response) => {
          return response.data;
      });  
      
      return guess;   
    },

    async updateChallengesDone(state, payload){
      axios({
      method: "put",
      url: url + "user/" + this.getters.getUserId,
      data: {
        challenge_id: payload
      },
      headers: {
        Authorization: `Bearer ${this.getters.getToken}`,
      },
    })
      .then(() => {
        console.log("Password sent")
      })
      .catch((error) => {
        console.log(error);
      });},

    async loadChallengesDone(){
      axios({
      method: "get",
      url: url + "user/challengesdone/" + this.getters.getUserId,
      params: {
        userId: this.getters.getUserId,
      },
      headers: {
        Authorization: `Bearer ${this.getters.getToken}`,
      },
    })
      .then((response) => {
        if (response.data != "") {
          this.commit("setListeChallengesDone", response.data) ;
        }
      })
      .catch((error) => {
        console.log(error);
      });},

      async rollback(){
      axios({
      method: "get",
      url: url + "rollback",
      headers: {
        Authorization: `Bearer ${this.getters.getToken}`,
      },
    })
      .then((response) => {
        if (response.data != "") {
          this.commit("setListeChallengesDone", response.data) ;
        }
      })
      .catch((error) => {
        console.log(error);
      });}

  },
  modules: {},
  getters : {
    getUserId: state => state.userid,
    getPasswordGuess: state => state.passwordGuess,
    getToken: state => state.token,
    getChallenges: state => state.listeChallenges,
    getChallengesDone: state => state.listeChallengesDone,
  },
   plugins: [createPersistedState({
    storage: {
      getItem: key => Cookies.get(key),
      setItem: (key, value) => Cookies.set(key, value, { expires: 3, secure: true }),
      removeItem: key => Cookies.remove(key)
    }
  })],
});
