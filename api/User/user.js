// Initializing all of the requires we need
const mongoose = require("mongoose");
var express = require("express");
var router = express.Router();
var fs = require("fs");
const { Encode } = require("../JwtAuth/encodage");
const { Decode } = require("../JwtAuth/decodage");

const user = require("../Models/userModel.js");


var users = new user({
  username: "Nicolas",
  password: "Test",
  taskforce: "1",
  challenges_done : []
});

//inserting into the database if the document doesn't exist
user.exists({
  user
}, (err, res) => {
  if (res) {
    console.log("The database is already Populated by users, no changes done");
  } else {
    user.insertMany(users, (err, res) => {
      console.log(res);
    });
  }
});

async function finduser(filter) {
  const result = await user.find(filter).populate({path: 'challenges_done'});
  return result;
}

async function findChallengesDone(filter) {
  const result = await user.findById(filter).populate({path: 'challenges_done', select: '_id'});
  return result;
}

//Route to get a list of all of the users
router.get("/", async (req, res) => {
  if (req.headers.authorization != undefined) {
    var isValid = Decode(req.headers.authorization);
  } else {
    res.send("Token non fourni");
  }

  if (isValid) {
   let users = await finduser({}).then((data) => {
     return data;
   });
   res.send(users)
  } else {
    res.send("Token not valid");
  }
});

//Route to get a list of all of the users and their points
router.get("/challengesdone/:id", async (req, res) => {
  if (req.headers.authorization != undefined) {
    var isValid = Decode(req.headers.authorization);
  } else {
    res.send("Token non fourni");
  }

  if (isValid) {
    let userId = req.params.id;
   let users = await findChallengesDone(userId).then((data) => {
     return data;
   });
   res.send(users)
  } else {
    res.send("Token not valid");
  }
});

router.post("/login", (req, res) => {
  var Payload = {
    iss: "taskforce",
    sub: "challenges_token",
    aud: req.body.username,
    name: req.body.username,
  };
  user.find(
    { username: req.body.username, password: req.body.password },
    (err, response) => {
      if (err) {
        res.send("User not found, Password or Name does not match");
      } else if (response == null || response.length) {
        console.log(response[0]._id);
        try {
          var token = Encode(Payload);
        } catch (error) {
          console.log(error.message);
        }
        res.send({token : token, id : response[0]._id});
      } else {
        console.log(req.body)
        res.send("User not found");
      }
    }
  );
});

//Route to add a new user to the Database
router.post("/", (req, res) => {
  var userSchema = new user({
    username: req.body.username,
    password: req.body.password,
    taskforce: req.body.taskforce,
    challenges_done: [],
  });

  user.insertMany(userSchema, (err, result) => {
    if (err) {
      res.send(" Creation impossible " + err);
    } else {
      res.send(userSchema + " Créé ");
    }
  });
});

//route to update a user
router.put("/:id", (req, res) => {
  if (req.headers.authorization != undefined) {
    var isValid = Decode(req.headers.authorization);
  } else {
    res.send("Token non fourni");
  }

  if (isValid) {
    console.log(req.body)
    user.findByIdAndUpdate(
      req.params.id ,
      { $push: { challenges_done: [req.body.challenge_id] } },
      (err, raw) => {
        if (err) {
          res.send(err);
        } else {
          res.send(raw + "Updated");
        }
      }
    );
  }
});

module.exports = router;
