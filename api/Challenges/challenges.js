// Initializing all of the requires we need
const mongoose = require("mongoose");
var express = require("express");
var router = express.Router();
var fs = require("fs");
const { Decode } = require("../JwtAuth/decodage");

const challenges = require("../Models/challengesModel.js");
const user = require("../Models/userModel.js");

async function challengesfind(filter) {
  const result = await challenges.find(filter).populate();
  return result;
}

var challenge = new challenges({
  name: "ChallengeTest",
  description: "ChallengeTest",
  link: "http",
  password: "Pass1",
});

//inserting into the database if the document doesn't exist
challenges.exists({
  challenges
}, (err, res) => {
  if (res) {
    console.log("The database is already Populated by challenges, no changes done");
  } else {
    challenges.insertMany(challenge, (err, res) => {
      console.log(res);
    });
  }
});

//Route to get a list of all of the users and their points
router.get("/", async (req, res) => {
  if (req.headers.authorization != undefined) {
    var isValid = Decode(req.headers.authorization);
  } else {
    res.send("Token non fourni");
  }

  if (isValid) {
    const challengesresult = await challengesfind({}).then((data) => {
      return data;
    });
    res.send(challengesresult);
  } else {
    res.send("Token not valid");
  }
});

router.post("/password", async (req, res) => {
  if (req.headers.authorization != undefined) {
    var isValid = Decode(req.headers.authorization);
  } else {
    res.send("Token non fourni");
  }

  if (isValid) {
    console.log(req.body)
    var correct = false;
    var password = req.body.password;

    challenges.findById(req.body.challengeid, (err, resu) => {
      if(err){
        res.send(err)
      }
      else if(resu.password == password){
        correct = true
      }
      res.send(true)
    })
  } else {
    res.send("Token not valid");
  }
});

module.exports = router;
