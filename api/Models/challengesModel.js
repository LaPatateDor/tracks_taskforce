//Require Mongoose
var mongoose = require("mongoose");

//Define a schema
var Schema = mongoose.Schema;

var challengesSchema = new Schema({
  name: String,
  description: String,
  link: String,
  password: String,
});

//Export function to link to user
module.exports = mongoose.model("Challenges", challengesSchema);
