//Require Mongoose
var mongoose = require("mongoose");

//Define a schema
var Schema = mongoose.Schema;

var userSchema = new Schema({
  username: String,
  password: String,
  taskforce: String,
  challenges_done: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Challenges",
    },
  ],
});

//Export function to create "Users" model class
module.exports = mongoose.model("Users", userSchema);
