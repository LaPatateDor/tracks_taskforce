// Adding all of the requires we will use
const express = require("express");
const app = express();
var cors = require('cors')
const path = require("path");
const http = require("http").createServer(app);
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const { default: axios } = require("axios");
require('https').globalAgent.options.ca = require('ssl-root-cas').create();
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0
const { Decode } = require("./JwtAuth/decodage");

// Joining the different js module parts of the armor
const challenges = require(path.join(__dirname, "/Challenges/challenges.js"));
const user = require(path.join(__dirname, "/User/user.js"));

app.use(cors());
// Adding the IHM access allow
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

async function makeRequest() {

var first =  await axios({
        method: "post",
        url: "https://track13-ops01.track-campus.academy:8006/api2/json/access/ticket",
        data: { 
          username: "administrateur@pam",
          password: "--Track-13-OPS01-jn3wr6UV--",
        },
      }).then((response) => {
        console.log(response.data)
       return response.data
      }).catch((err) => {
        console.log(err);
      });
      var cookie = "PVEAuthCookie="+first.data.ticket+"; Path=/; Domain=track13-ops01.track-campus.academy;";
      var header = first.data.CSRFPreventionToken

      axios({
        method: "post",
        url: "https://track13-ops01.track-campus.academy:8006/api2/json/nodes/TRACK13-OPS01/qemu/600/snapshot/sudo2/rollback",
        headers:{ 
          Cookie: cookie,
          CSRFPreventionToken: header
        } ,
      }).then((response) => {
        console.log(response)
      }).catch((err) => {
        console.log(err);
      });
}


app.get("/rollback", (req, res) => {
   if (req.headers.authorization != undefined) {
    var isValid = Decode(req.headers.authorization);
  } else {
    res.send("Token non fourni");
  }
   if (isValid) {
    makeRequest()
    res.send("Rollback fait")
   }
   else{
     res.send("Token non valide")
   }
});


//Setting up the database connection
mongoose.connect("mongodb://interface-db:27017/taskforce", {
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
});
const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", function () {
  console.log("Connected to the database");
});

//Declaring the static dirname of our project
app.use(express.static(path.join(__dirname, "/")));

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

// Setting the different routes for all of the armor parts and for the armor
app.use("/user", user);
app.use("/challenges", challenges);

// Default get when no routes are called
app.get("/", (req, res) => {
  res.send("Voici la page d'accueil");
});

http.listen(3000);
