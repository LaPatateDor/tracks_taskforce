const { default: base64url } = require("base64url");
const base64 = require("base64url");
const crypto = require("crypto");
const fs = require("fs");

const sourceFile = require("./encodage.js");

function headerVerify(header) {
  if (header.alg != "RS256" || header.typ != "JWT") {
    return false;
  } else {
    return true;
  }
}

function payloadVerify(payload) {
  const issuers = ["taskforce"];

  var current_time = new Date().getTime() / 1000;
  if (current_time > payload.exp) {
    return false;
  } else if (!issuers.includes(payload.iss)) {
    return false;
  } else {
    return true;
  }
}

function DecodeJwt(token) {
  const verifyFunction = crypto.createVerify("RSA-SHA256");
  const tokenBear = token.split(" ");
  const bearerToken = tokenBear[1];

  const PUB_KEY = fs.readFileSync("JwtAuth/jwtRS256.key.pub", "utf8");
  const jwtHeader = bearerToken.split(".")[0];
  const jwtPayload = bearerToken.split(".")[1];
  const jwtSignature = bearerToken.split(".")[2];

  verifyFunction.write(jwtHeader + "." + jwtPayload);
  verifyFunction.end();

  const jwtSignatureBase64 = base64.toBase64(jwtSignature);
  var signatureIsValid;
  if (signatureIsValid == null) {
    signatureIsValid = verifyFunction.verify(
      PUB_KEY,
      jwtSignatureBase64,
      "base64"
    );
  }

  console.log("La signature est valide : " + signatureIsValid);

  var headerJson = JSON.parse(base64.decode(jwtHeader));
  var payloadJson = JSON.parse(base64.decode(jwtPayload));

  console.log("Le header est valide : " + headerVerify(headerJson));
  console.log(headerJson);

  console.log("Le payload est valide : " + payloadVerify(payloadJson));
  console.log(payloadJson);
  if (payloadJson && headerJson && signatureIsValid) {
    return true;
  }
}

module.exports = {
  Decode: DecodeJwt,
};
